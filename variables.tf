variable "aws_region" {
  description = "AWS region"
  type        = string
}

variable "aws_profile" {
  description = "AWS profile"
  type        = string
  default     = "default"
}

variable "bucket" {
  description = "AWS Bucket"
  type        = string
}
