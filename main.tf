resource "aws_s3_bucket" "amis_bucket" {
  bucket = var.bucket
}

data "aws_iam_policy_document" "assume_role_document" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["vmie.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
    condition {
      test     = "StringEquals"
      variable = "sts:Externalid"
      values = [
        "vmimport"
      ]
    }
  }
}

resource "aws_iam_role" "vmimport_role" {
  assume_role_policy = data.aws_iam_policy_document.assume_role_document.json
}

data "aws_iam_policy_document" "vmimmport_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${var.bucket}",
      "arn:aws:s3:::${var.bucket}/*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:GetBucketAcl"
    ]
    resources = [
      "arn:aws:s3:::export-bucket",
      "arn:aws:s3:::export-bucket/*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "ec2:ModifySnapshotAttribute",
      "ec2:CopySnapshot",
      "ec2:RegisterImage",
      "ec2:Describe*"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "vmimport_policy" {
  policy = data.aws_iam_policy_document.vmimmport_policy_document.json
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment" {
  role       = aws_iam_role.vmimport_role.name
  policy_arn = aws_iam_policy.vmimport_policy.arn
}
